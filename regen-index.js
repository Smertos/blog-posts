const { createHash } = require('crypto');
const { readdir, readFile, writeFile } = require('fs');
const { promisify } = require('util');

const asyncReaddir = promisify(readdir);
const asyncRead = promisify(readFile);
const asyncWrite = promisify(writeFile);

const indexFilename = 'index.json';

const digest = content => createHash('sha256').update(content, 'utf-8').digest().toString('base64');
const handleError = (actionDesc, errorCode, err) => {
    console.error(err);
    console.log(`Failed to ${actionDesc}. Terminating...`);
    process.exit(errorCode);
};

asyncRead(indexFilename)
    .then(contents => {
        try {
            const parsed = JSON.parse(contents);
            return parsed;
        } catch (err) {
            handleError('parse index file', -2, err);
        }
    })
    .catch(handleError.bind(null, 'read index file', -1))
    .then(({ posts }) => [posts, Object.keys(posts)])
    .then(d => Promise.all([ registerNewPosts(d), updateExistingPosts(d) ]))
    .then(([ newPosts, updatedPosts ]) => {
        const result = {};

        console.debug(newPosts, updatedPosts);
        
        newPosts.concat(updatedPosts).forEach(post => {
            Object.assign(result, { [post.name]: Object.assign({}, post, { name: void 0 }) });
        });
        
        return { posts: result };
    })
    .then(indexObj => asyncWrite(indexFilename, JSON.stringify(indexObj, null, 4)))
    .catch(handleError.bind(null, 'write index file', -3));

function registerNewPosts([ posts, postNames ]) {
    return asyncReaddir('.')
        .then(files => files.filter(
            file => file.endsWith('.md') && !postNames.includes(file.replace('.md', ''))
        ))
        .then(files => Promise.all(files.map(
            file => asyncRead(file).then(contents => ({
                name: file.replace('.md', ''),
                created: new Date().toString(),
                hash: digest(contents)
            }))
        )))
        .then(posts => {
            console.log(`Registered ${posts.length} new posts`);
            return posts;
        });
}

function updateExistingPosts([ posts, postNames ]) {
    return Promise
        .all(postNames.map(name => updatePost(name, posts[name])))
        .then(posts => {
            console.log(`Updated ${posts.length} existing posts`);
            return posts;
        });
}

function updatePost(name, post) {
    return asyncRead(`${name}.md`)
        .then(contents => {
            const hash = digest(contents);
            
            if (post.hash !== hash) {
                post.hash = hash;
                post.updated = new Date().toString();
            }

            return Object.assign({ name }, post);
        });
}
